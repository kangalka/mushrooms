package com.example;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.data.ItemClass;
import com.example.data.ItemClassViewModal;
import com.example.mushrooms.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.text.TextUtils.isEmpty;


public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ListViewHolder> { //указали, что адаптер будет работать с нашим вьюхолдером)

    private List<ItemClass> items;
    ItemClassViewModal itemClassViewModal;
    private Context context;

    public void setRow_index(int row_index) {
        this.row_index = row_index;
    }

    int row_index = -1;


    public MyListAdapter(OnItemListener onItemListener) {

        this.onItemListener = onItemListener;
    }


    public ItemClass getItem(int index) {
        return items.get(index);
    }
    //создаем интерфейс слушатель для редактирования и удаления

    private OnItemListener onItemListener;

    public interface OnItemListener {
        public void onClickItem(ItemClass item, int i);

        public void onSwipeRight(int i);

        public void onSwipeLeft(int i);
    }


    void setData(List<ItemClass> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    //2. Создаем вью холдер
    @NonNull
    @Override
    public MyListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_item_list, viewGroup, false);

        //true - вью будет создана и сразу добавлена в разметку
        //false - не сразу добавлена в разметку нужно использовать когда родитель (view group) пока еще не создан

        MyListAdapter.ListViewHolder viewHolder = new ListViewHolder(itemview);

        return viewHolder;
    }

    //3. Устанавляваем связку между классом вьюхолдером и нашим классом с данными
    @Override
    public void onBindViewHolder(@NonNull MyListAdapter.ListViewHolder viewHolder, final int position) {
        final ItemClass item = items.get(position);
        if (!(isEmpty(item.getPhoto()))) {
            ArrayList currentPhotos = ItemClass.formatPhoto(item.getPhoto());
            Picasso.get().load(currentPhotos.get(0).toString()).into(viewHolder.ivPhoto);
        } else {
            viewHolder.ivPhoto.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
        }
        viewHolder.tvName.setText(item.getName());
        viewHolder.tvCategory.setText(item.getCategory());
        viewHolder.tvPeriod.setText(item.getStartDate() + " - " + item.getEndDate());

        viewHolder.clItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
                onItemListener.onClickItem(item, position);


            }


        });


        if (row_index == position) {
            viewHolder.clItem.setBackgroundResource(R.drawable.press_item_background);
        } else {
            viewHolder.clItem.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }

    // подсчет количества элементов
    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    // 1. Создаем класс, содержащий один элемент здесь храним ссылку на вью (или отдельные компоненты вью,
    // из которой надо показать данные, т.е. вью с одним элементом
    public class ListViewHolder extends RecyclerView.ViewHolder {
        public View clItem;
        public ImageView ivPhoto;
        public TextView tvName;
        public TextView tvCategory;
        public TextView tvPeriod;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            clItem = itemView.findViewById(R.id.clItem);
            ivPhoto = itemView.findViewById(R.id.aivFoto);
            tvName = itemView.findViewById(R.id.atvName);
            tvCategory = itemView.findViewById(R.id.atvCategory);
            tvPeriod = itemView.findViewById(R.id.atvPeriod);
        }
    }
}
