package com.example.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

;
@Database(entities = {ItemClass.class},version = 1, exportSchema = false)

public abstract class ItemClassDataBase extends RoomDatabase {
    abstract  ItemClassDAO getItemClassDAO();

  //делаем синглтон
    private static ItemClassDataBase instance;
    public static ItemClassDataBase getInstance(Context context){
       if (instance == null) {
           instance = Room.databaseBuilder(context.getApplicationContext(),
                   ItemClassDataBase.class, "db_item")
                   .build();

       }

       return instance;
    }
}



