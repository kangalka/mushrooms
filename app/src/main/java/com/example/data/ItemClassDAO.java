package com.example.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ItemClassDAO {

    @Insert
    void firstLoad(List<ItemClass> items);

    @Insert
    void add (ItemClass item);

    @Update
    void edit (ItemClass item);

    @Delete
    void delete (ItemClass item);


    @Query("SELECT*FROM item_class_table")
    LiveData<List<ItemClass>> getAll();

    @Query("SELECT * FROM item_class_table WHERE id = :id")
    ItemClass getById(int id);


    @Query("delete from item_class_table")
    void deleteAll();


}
