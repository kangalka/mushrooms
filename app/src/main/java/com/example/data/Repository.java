package com.example.data;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class Repository {
    private static Repository instance;
    private static LiveData<List<ItemClass>> data;
    private static ItemClassDAO itemClassDAO;
    private Repository(){

    }
    public static Repository getInstance(Context context){
        if(instance == null){
            instance = new Repository();

        }
        itemClassDAO = ItemClassDataBase.getInstance(context).getItemClassDAO();
        return instance;
    }

    public LiveData<List<ItemClass>> getAll()
    {

        return itemClassDAO.getAll();
    }

    public void firstLoad(List<ItemClass> items){

        new AsyncTask<List<ItemClass>,Void, Void>(){
            @Override
            protected Void doInBackground(List<ItemClass>... items) {
                itemClassDAO.firstLoad(items[0]);
                return null;
            }
        }.execute(items);

    }

    public void add(ItemClass item){

        new AsyncTask<ItemClass,Void, Void>(){
            @Override
            protected Void doInBackground(ItemClass... items) {
                itemClassDAO.add(items[0]);
                return null;
            }
        }.execute(item);

    }

    public void edit(ItemClass item){
        new AsyncTask<ItemClass,Void, Void>(){
            @Override
            protected Void doInBackground(ItemClass... items) {
                itemClassDAO.edit(items[0]);
                return null;
            }
        }.execute(item);
    }

    public void delete(ItemClass item){
        new AsyncTask<ItemClass,Void, Void>(){
            @Override
            protected Void doInBackground(ItemClass... items) {
                itemClassDAO.delete(items[0]);
                return null;
            }
        }.execute(item);

    }
    public void deleteByIndex(int index){
        delete(getAll().getValue().get(index));
    }

    public ItemClass getItemByIndex(int index) {
       return getAll().getValue().get(index);

    }


    public void deleteAll(){
        new AsyncTask<Void,Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                itemClassDAO.deleteAll();
                return null;
            }
        }.execute();

    }

    /*public void deletePhoto(){
        new AsyncTask<Void,Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                itemClassDAO.deletePhoto();
                return null;
            }
        }.execute();

    }
*/
}
