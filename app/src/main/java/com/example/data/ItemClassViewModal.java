package com.example.data;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class ItemClassViewModal extends AndroidViewModel {

    public int row_index;

    private Repository repository = Repository.getInstance(getApplication());

    public ItemClassViewModal(@NonNull Application application) {
        super(application);
    }

    public void firstLoad(List<ItemClass> items) {
        repository.firstLoad(items);
    }

    public void add(ItemClass item) {
        repository.add(item);
    }

    public void edit(ItemClass item) {
        repository.edit(item);
    }

    public void delete(ItemClass item) {
        repository.delete(item);
    }

    public void deleteByIndex(int index) {
        repository.deleteByIndex(index);
    }

    public ItemClass getItemByIndex (int index) { return repository.getItemByIndex(index);}


    public LiveData<List<ItemClass>> getAll() {
        return repository.getAll();
    }

    public void deleteAll() {
        repository.deleteAll();
    }


}

