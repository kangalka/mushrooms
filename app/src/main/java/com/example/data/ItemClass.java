package com.example.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.util.Log;

import com.example.mushrooms.R;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static android.text.TextUtils.isEmpty;


@Entity(tableName = "item_class_table")
public class ItemClass {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String photo;
    private String name;
    private String category;
    private int maxSize;
    private int size;
    private String startDate;
    private String endDate;
    private String fullDescription;
    private String formula;


    public ItemClass(int id, String photo, String name, String category, int maxSize, int size, String startDate, String endDate, String fullDescription, String formula) {
        this.id = id;
        this.photo = photo;
        this.name = name;
        this.category = category;
        this.maxSize = maxSize;
        this.size = size;
        this.startDate = startDate;
        this.endDate = endDate;
        this.fullDescription = fullDescription;
        this.formula = formula;
    }

    @Ignore
    public ItemClass(String photo, String name, String category, int maxSize, int size, String startDate, String endDate, String fullDescription, String formula) {
        this.photo = photo;
        this.name = name;
        this.category = category;
        this.maxSize = maxSize;
        this.size = size;
        this.startDate = startDate;
        this.endDate = endDate;
        this.fullDescription = fullDescription;
        this.formula = formula;
    }


    public static ArrayList<String> formatPhoto(String photo) {
        ArrayList<String> photos = new ArrayList<String>();

        if (!(isEmpty(photo))) {
            Pattern p = Pattern.compile("\\,");
            String[] temp = p.split(photo);
            for (String t : temp
            ) {
                photos.add(t);
            }
        }
        return photos;
    }


    public static ArrayList<ItemClass> loadItem(Context context) {
        ArrayList<ItemClass> data = new ArrayList<>();
        String[] photos = context.getResources().getStringArray(R.array.photo);
        String[] names = context.getResources().getStringArray(R.array.name);
        String[] categories = context.getResources().getStringArray(R.array.category);
        int[] maxSizes = context.getResources().getIntArray(R.array.max_size);
        int[] sizes = context.getResources().getIntArray(R.array.size);
        String[] startDates = context.getResources().getStringArray(R.array.start_date);
        String[] endDates = context.getResources().getStringArray(R.array.end_date);
        String[] fullDescriptions = context.getResources().getStringArray(R.array.fulll_description);
        String[] formulas = context.getResources().getStringArray(R.array.formula);

        for (int i = 0; i < names.length; i++) {
            data.add(new ItemClass(photos[i],
                    names[i],
                    categories[i],
                    maxSizes[i],
                    sizes[i],
                    startDates[i],
                    endDates[i],
                    fullDescriptions[i],
                    formulas[i]));
        }

        Log.d("tag", "onCreate data: " + data.toString());
        return data;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }


}
