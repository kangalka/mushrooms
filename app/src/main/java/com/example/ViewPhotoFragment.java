package com.example;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.data.ItemClass;
import com.example.mushrooms.R;

import java.util.ArrayList;


public class ViewPhotoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String PHOTO = "photo";

    private String photo;
    public static ArrayList<String> photos;

    public ViewPhotoFragment() {
        // Required empty public constructor
    }


    public static ViewPhotoFragment newInstance(ItemClass item, String photo) {
        ViewPhotoFragment fragment = new ViewPhotoFragment();
        Bundle args = new Bundle();
        args.putString(PHOTO, item.getPhoto().toString());

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            photo = getArguments().getString(PHOTO);
        }

        photos = ItemClass.formatPhoto(photo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_photo, container, false);

    }



    public static class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
           // return ViewPhotoFragment.newInstance(item, "https://kvetok.ru/wp-content/uploads/2016/04/0404a-91.jpg");
            return null;
        }
        @Override
        public int getCount()
        {
            if (ViewPhotoFragment.photos != null) {
                return ViewPhotoFragment.photos.size();
            }
            return 0;
        }
    }
}
