package com.example;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.mushrooms.R;
import com.squareup.picasso.Picasso;

public class SplashActivity extends AppCompatActivity {
    ObjectAnimator textAnim;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo = findViewById(R.id.ivStar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        animText();
        animRotate();


    }


    void startMainScreen() {
        Intent intent = (new Intent(this, MainList.class));
        startActivity(intent);
        Log.d("tag", "startActivity: ");
        finish();
        overridePendingTransition(R.anim.scale_fill_start, R.anim.scale_null_end);
        Log.d("tag", "overridePendingTransition: ");
    }

    public void animText() {

        textAnim = ObjectAnimator.ofArgb(findViewById(R.id.tvWelcom), "textColor", 0, 0xffFFFF00);
        textAnim.setDuration(3500);


        textAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startMainScreen();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        textAnim.start();
        Log.d("tag", "animText: ");


    }

    void animRotate() {
        Animation starAnim = AnimationUtils.loadAnimation(this, R.anim.scale_center);
        starAnim.setDuration(3500);

        Picasso.get().load("http://vsegriby.ru/assets/images/009/1034.jpg").into(logo);
        logo.startAnimation(starAnim);
        Log.d("tag", "animRotate: ");


    }


}



