package com.example;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.example.mushrooms.R;

public class DeleteAlert extends DialogFragment {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String ITEM_POSITION = "item_position";


    private String title;
    private String message;
    private int itemPosition;

    public DeleteAlert() {
        // Required empty public constructor
    }


    public static DeleteAlert newInstance(String title, String message, int itemPosition) {
        DeleteAlert fragment = new DeleteAlert();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        args.putInt(ITEM_POSITION, itemPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(TITLE);
            message = getArguments().getString(MESSAGE);
            itemPosition = getArguments().getInt(ITEM_POSITION);
        }

      /*  try {
            Callback callback = (Callback) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement Callback interface");
        }*/
    }


    public interface DeleteDialogOkListener {
        public void onOkClicked(int itemPosition);
    }

    DeleteDialogOkListener dialogOkListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setView(R.layout.delete_dialog)
                .setIcon(R.drawable.ic_add_alert_black_24dp)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (dialogOkListener != null&&title.equals("Удаление записи")) {
                            dialogOkListener.onOkClicked(itemPosition);
                        } else {

                        Intent intent = new Intent();
                        intent.putExtra("pozition", itemPosition);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        dialogOkListener = ((DeleteDialogOkListener) context);

        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        dialogOkListener = null;
    }
}