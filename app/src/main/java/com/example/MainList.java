package com.example;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.example.data.ItemClass;
import com.example.data.ItemClassViewModal;
import com.example.mushrooms.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.support.v7.widget.helper.ItemTouchHelper.LEFT;
import static android.support.v7.widget.helper.ItemTouchHelper.RIGHT;


public class MainList extends AppCompatActivity implements MyListAdapter.OnItemListener, DeleteAlert.DeleteDialogOkListener {
    ArrayList<ItemClass> data;
    public MyListAdapter listAdapter;
    String flag = "show";
    RecyclerView recyclerView;
    ItemClassViewModal itemClassViewModal;
    SharedPreferences prefs = null;

    int row_index = -1;

    public static boolean isPortraitOrientation(Context context) {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            return true;
        } else {
            return false;
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);

        //через адаптер отображаем список в ресайклер вью
        listAdapter = new MyListAdapter(this);
        recyclerView = findViewById(R.id.rvMainList);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        swipeListener(recyclerView);

        //устанавливаем слушатель на изменения в базе данных
        itemClassViewModal = ViewModelProviders.of(this).get(ItemClassViewModal.class);
        itemClassViewModal.getAll().observe(this, new Observer<List<ItemClass>>() {
            @Override
            public void onChanged(List<ItemClass> items) {
                listAdapter.setData(items);
            }
        });


//при первом запуске подгружаем исходную базу данных
        prefs = getSharedPreferences("com.example.mushrooms", MODE_PRIVATE);
        if (prefs.getBoolean("firstrun", true)) {
            data = ItemClass.loadItem(this);
            itemClassViewModal.firstLoad(data);
            prefs.edit().putBoolean("firstrun", false).commit();
        }


//при первоначальной загрузке списка в альбомной ориентации  открываем фрагмент для первого элемента
        Log.d("tag", "row_id onCreate" + row_index);
        if (!isPortraitOrientation(this)) {
           /* row_index = 0;

            if (savedInstanceState == null) {
                ItemFragment itemFragment = ItemFragment.newInstance(itemClassViewModal.getAll().getValue().get(row_index), flag, row_index);
                FragmentManager fm = getSupportFragmentManager();

                fm.beginTransaction()
                        .add(R.id.fragment, itemFragment)
                        .commit();


                ((MyListAdapter) Objects.requireNonNull(recyclerView.getAdapter())).setRow_index(row_index);
                recyclerView.getAdapter().notifyDataSetChanged();*/
            }

// при повороте экрана в альбомную ориентацию - открываем фрагмент для выбранной позиции
            if (savedInstanceState != null) {
                row_index = itemClassViewModal.row_index;
            /*ItemFragment itemFragment = ItemFragment.newInstance(itemClassViewModal.getAll().getValue().get(row_index), flag, row_index);
            FragmentManager fm = getSupportFragmentManager();
            if (!isPortraitOrientation(this)) {
                fm.beginTransaction()
                        .add(R.id.fragment, itemFragment)
                        .commit();
            }*/
                ((MyListAdapter) Objects.requireNonNull(recyclerView.getAdapter())).setRow_index(row_index);
                recyclerView.getAdapter().notifyDataSetChanged();

            }
        
    }


    public void onAddItem(View view) {
        flag = "add";
        //сделать через перегруженный метод
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, ItemFragment.newInstance(flag))
                .addToBackStack("item")
                .commit();
    }


    public void swipeListener(final RecyclerView rv) {
        ItemTouchHelper.SimpleCallback itSimpleCallback = new ItemTouchHelper.SimpleCallback(0, RIGHT | LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == RIGHT) {
                    int position = viewHolder.getAdapterPosition();
                    onSwipeRight(position);
                    itemClassViewModal.row_index = position;
                    ((MyListAdapter) Objects.requireNonNull(rv.getAdapter())).setRow_index(position);
                    rv.getAdapter().notifyDataSetChanged();

                } else if (direction == LEFT) {
                    int position = viewHolder.getAdapterPosition();
                    onSwipeLeft(position);
                    Objects.requireNonNull(rv.getAdapter()).notifyDataSetChanged();
                }

            }

        };
        new ItemTouchHelper(itSimpleCallback).attachToRecyclerView(rv);
    }


    //методы, переопределенные от resyclerview
    @Override
    public void onClickItem(ItemClass item, int i) {
        flag = "show";
        itemClassViewModal.row_index = i;
        ItemFragment itemFragment = ItemFragment.newInstance(item, flag, i);
        FragmentManager fm = getSupportFragmentManager();
        if (!isPortraitOrientation(this)) {
            fm.beginTransaction()
                    .replace(R.id.fragment, itemFragment)
                    .commit();
        } else {
            fm.beginTransaction()
                    .add(R.id.fragment, itemFragment)
                    .addToBackStack("item") // .addToBackStack(null) - чтобы фрагмент удалился
                    .commit();
        }

    }

    @Override
    public void onSwipeRight(int i) {

        flag = "edit";
        if (listAdapter.getItem(i) == null) return;
        ItemFragment itemFragment = ItemFragment.newInstance(listAdapter.getItem(i), flag, i);
        FragmentManager fm = getSupportFragmentManager();

        if (!isPortraitOrientation(this)) {
            fm.beginTransaction()
                    .replace(R.id.fragment, itemFragment)
                    .addToBackStack("item")
                    .commit();
        } else {
            fm.beginTransaction()
                    .add(R.id.fragment, itemFragment)
                    .addToBackStack("item")
                    .commit();
        }

    }

    @Override
    public void onSwipeLeft(int i) {
        DeleteAlert deleteAlert = DeleteAlert.newInstance("Удаление записи", "Вы действительно хотите удалить запись?", i);
        deleteAlert.show(getSupportFragmentManager(), "dlg");
    }

    @Override
    public void onBackPressed() {
        if (isPortraitOrientation(this)) {
            if (getSupportFragmentManager().getFragments().size() != 0) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
                assert fragment != null;
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onOkClicked(int i) {
        if (listAdapter.getItem(i) == null) return;
        itemClassViewModal.delete(listAdapter.getItem(i));

    }
}

