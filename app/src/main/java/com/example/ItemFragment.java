package com.example;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.data.ItemClass;
import com.example.data.ItemClassViewModal;
import com.example.mushrooms.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static java.lang.String.valueOf;


public class ItemFragment extends Fragment implements DeleteAlert.DeleteDialogOkListener {

    Uri uri;
    ArrayList<String> currentPhotos;
    View v;

    ItemClassViewModal itemClassViewModal;

    MutableLiveData<ArrayList<String>> live_photos = new MutableLiveData<>();
    int idCamera = 3000;

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHOTO = "photo";
    private static final String CATEGORY = "category";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final String SIZE = "size";
    private static final String MAX_SIZE = "maxSize";
    private static final String FULL_DESCRIPTION = "fullDescription";
    private static final String FORMULA = "formula";
    private static final String FLAG = "flag";
    private static final String POSITION = "position";


    private int id;
    private String photo;
    private String name;
    private String category;
    private String startDate;
    private String endDate;
    private int size;
    private int maxSize;
    private String fullDescription;
    private String formula;
    private String flag;
    private int position;


    public ItemFragment() {
        // Required empty public constructor
    }


    public static ItemFragment newInstance(ItemClass item, String flag, int position) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ID, item.getId());
        args.putInt(POSITION, position);
        args.putString(FLAG, flag);
        args.putString(NAME, item.getName());
        args.putString(PHOTO, item.getPhoto().toString());
        args.putString(CATEGORY, item.getCategory());
        args.putInt(SIZE, item.getSize());
        args.putInt(MAX_SIZE, item.getMaxSize());
        args.putString(START_DATE, item.getStartDate());
        args.putString(END_DATE, item.getEndDate());
        args.putString(FULL_DESCRIPTION, item.getFullDescription());
        args.putString(FORMULA, item.getFormula());
        fragment.setArguments(args);
        return fragment;
    }


    public static ItemFragment newInstance(String flag) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(FLAG, flag);
        fragment.setArguments(args);
        Log.d("tag", "FLAG IN ADD " + flag);
        return fragment;
    }

    private void spinnerSelectValue(Spinner spinner, Object value) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        itemClassViewModal = ViewModelProviders.of(getActivity()).get(ItemClassViewModal.class);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            id = getArguments().getInt(ID);
            name = getArguments().getString(NAME);
            photo = getArguments().getString(PHOTO);
            category = getArguments().getString(CATEGORY);
            size = getArguments().getInt(SIZE);
            maxSize = getArguments().getInt(MAX_SIZE);
            startDate = getArguments().getString(START_DATE);
            endDate = getArguments().getString(END_DATE);
            fullDescription = getArguments().getString(FULL_DESCRIPTION);
            formula = getArguments().getString(FORMULA);
            flag = getArguments().getString(FLAG);
            position = getArguments().getInt(POSITION);

        }

        currentPhotos = ItemClass.formatPhoto(photo);
        setRetainInstance(true); // метод фрагмента, для того чтобы при повороте фрагмент не умирал с активностью, но при этом фрагмент не должен быть  стэке
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_item, container, false);

        //обработка фотографий


        for (int i = 0; i < currentPhotos.size(); i++
        ) {
            final int pozition = i;
            ImageView imagePhoto = new ImageView(getContext());
            int width = 360;
            LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.MATCH_PARENT);
            imageParams.setMargins(16, 12, 8, 8);
            imageParams.weight = 1;
            imagePhoto.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imagePhoto.setLayoutParams(imageParams);
            imagePhoto.setId(1000 + i);
            ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imagePhoto);
            Picasso.get().load(currentPhotos.get(i).toString()).into(imagePhoto);
            imagePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    getFragmentManager().beginTransaction()
                            .add(R.id.viewPager, ItemFragment.newInstance(flag))
                            .addToBackStack("item")
                            .commit();

                }

            });


            ImageView imageDelete = new ImageView(getContext());
            LinearLayout.LayoutParams imageDeleteParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageDeleteParams.setMargins(-24, -12, 0, 0);
            //     imageDeleteParams.weight = 1;
            imageDelete.setLayoutParams(imageDeleteParams);
            imageDelete.setId(2000 + i);
            ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imageDelete);
            imageDelete.setImageResource(android.R.drawable.btn_dialog);
            if (flag.equals("show")) imageDelete.setVisibility(View.GONE);

            imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!(flag.equals("show"))) {
                        DeleteAlert deleteAlert = DeleteAlert.newInstance("Удаление фотографии",
                                "Вы действительно хотите удалить фотографию?", pozition);
                        deleteAlert.setTargetFragment(ItemFragment.this, 3333);
                        deleteAlert.show(getFragmentManager(), "dlg");
                    }

                }

            });
        }

        ImageView imageCamera = new ImageView(getContext());
        int width = 240;
        LinearLayout.LayoutParams imageCameraParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.MATCH_PARENT);
        imageCameraParams.setMargins(16, 12, 8, 8);
        //  imageCameraParams.weight = 1;
        imageCamera.setLayoutParams(imageCameraParams);
        imageCamera.setId(idCamera);
        imageCamera.setScaleType(ImageView.ScaleType.FIT_CENTER);
        ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imageCamera);
        imageCamera.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
        if (flag.equals("show")) imageCamera.setVisibility(View.GONE);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag != "show") {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(intent, 1111);
                    }
                }
            }
        });


        live_photos.observe(this, new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> s) {
                s = currentPhotos;

                for (int i = 0; i < s.size(); i++
                ) {
                    ImageView imagePhotoLd = v.findViewById(1000 + i);
                    if (imagePhotoLd != null) {
                        Log.d("tag", " imageView: " + imagePhotoLd.toString());
                        Picasso.get().load(s.get(i).toString()).into(imagePhotoLd);
                    } else {
                        imagePhotoLd = new ImageView(getContext());
                        int width = 360;
                        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.MATCH_PARENT);
                        imageParams.setMargins(16, 12, 8, 8);
                        // imageParams.weight = 1;
                        imagePhotoLd.setLayoutParams(imageParams);
                        imagePhotoLd.setId(1000 + i);
                        imagePhotoLd.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imagePhotoLd);
                        Picasso.get().load(currentPhotos.get(i).toString()).into(imagePhotoLd);


                    }

                    ImageView imageDeleteLd = v.findViewById(2000 + i);
                    if (imageDeleteLd != null) {
                        imageDeleteLd.setImageResource(android.R.drawable.btn_dialog);
                        if (flag.equals("show")) imageDeleteLd.setVisibility(View.GONE);

                    } else {

                        imageDeleteLd = new ImageView(getContext());
                        LinearLayout.LayoutParams imageDeleteParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        imageDeleteParams.setMargins(-48, -12, 0, 0);
                        //  imageDeleteParams.weight = 1;
                        imageDeleteLd.setLayoutParams(imageDeleteParams);
                        imageDeleteLd.setId(2000 + i);
                        ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imageDeleteLd);
                        imageDeleteLd.setImageResource(android.R.drawable.btn_dialog);
                        if (flag.equals("show")) imageDeleteLd.setVisibility(View.GONE);

                    }
                    final int pozition = i;
                    imageDeleteLd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!(flag.equals("show"))) {
                                Log.d("OK delete", valueOf(pozition));
                                DeleteAlert deleteAlert = DeleteAlert.newInstance("Удаление фотографии",
                                        "Вы действительно хотите удалить фотографию?", pozition);
                                deleteAlert.setTargetFragment(ItemFragment.this, 3333);
                                deleteAlert.show(getFragmentManager(), "dlg");
                            }
                        }

                    });
                }


                ImageView imageCameraLd = v.findViewById(idCamera);
                if (flag.equals("show")) imageCameraLd.setVisibility(View.GONE);
                if (imageCameraLd != null) {
                    ((LinearLayout) v.findViewById(R.id.llPhotos)).removeView(imageCameraLd);
                    imageCameraLd = new ImageView(getContext());
                    int width = 240;
                    LinearLayout.LayoutParams imageCameraParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    imageCameraParams.setMargins(8, 12, 8, 8);
                    // imageCameraParams.weight = 1;
                    imageCameraLd.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageCameraLd.setLayoutParams(imageCameraParams);
                    imageCameraLd.setId(idCamera);
                    ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imageCameraLd);
                    imageCameraLd.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                    if (flag.equals("show")) imageCameraLd.setVisibility(View.GONE);
                    imageCameraLd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (flag != "show") {
                                Intent intent = new Intent(Intent.ACTION_PICK);
                                intent.setType("image/*");
                                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                    startActivityForResult(intent, 1111);
                                }
                            }
                        }
                    });


                } else {
                    imageCameraLd = new ImageView(getContext());
                    int width = 240;
                    LinearLayout.LayoutParams imageCameraParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    imageCameraParams.setMargins(16, 12, 8, 8);
                    imageCameraParams.weight = 1;
                    imageCameraLd.setLayoutParams(imageCameraParams);
                    imageCameraLd.setId(idCamera);
                    ((LinearLayout) v.findViewById(R.id.llPhotos)).addView(imageCameraLd);
                    imageCameraLd.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
                    if (flag.equals("show")) imageCameraLd.setVisibility(View.GONE);
                    imageCameraLd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (flag != "show") {
                                Intent intent = new Intent(Intent.ACTION_PICK);
                                intent.setType("image/*");
                                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                    startActivityForResult(intent, 1111);
                                }
                            }
                        }
                    });

                }

            }
        });


        final EditText edName = v.findViewById(R.id.fedName);
        edName.setText(name);
        final TextView stvName = v.findViewById(R.id.stvName);
        stvName.setText(name);

        final Spinner spCategory = v.findViewById(R.id.spCategory);
        spCategory.setSelection(0);
        if (flag.equals("edit")) spinnerSelectValue(spCategory, category);
        final TextView stvCategory = v.findViewById(R.id.stvCategory);
        if (flag.equals("show")) stvCategory.setText(category);

        final EditText edSize = v.findViewById(R.id.fedSize);

        edSize.setText(valueOf(size));
        final EditText edMaxSize = v.findViewById(R.id.fedMaxSize);
        edMaxSize.setText(valueOf(maxSize));
        final TextView stvSize = v.findViewById(R.id.stvSize);
        stvSize.setText(valueOf(size));
        final TextView stvMaxSize = v.findViewById(R.id.stvMaxSize);
        stvMaxSize.setText(valueOf(maxSize));

        final Spinner spStartDate = v.findViewById(R.id.spStartDate);
        final Spinner spEndDate = v.findViewById(R.id.spEndDate);
        spStartDate.setSelection(0);
        spEndDate.setSelection(0);
        if (flag.equals("edit")) {
            spinnerSelectValue(spStartDate, startDate);
            spinnerSelectValue(spEndDate, endDate);
        }

        final TextView stvPeriod = v.findViewById(R.id.stvPeriod);
        stvPeriod.setText(startDate + " - " + endDate);


        final EditText edFullDescription = v.findViewById(R.id.fedFullDescription);
        edFullDescription.setText(fullDescription);
        final TextView stvFullDescription = v.findViewById(R.id.stvFullDescription);
        stvFullDescription.setText(fullDescription);

        final EditText edFormula = v.findViewById(R.id.fedFormula);
        edFormula.setText(formula);
        final TextView stvFormula = v.findViewById(R.id.stvFormula);
        stvFormula.setText(formula);


        if (flag.equals("show")) {
            edName.setVisibility(View.GONE);
            stvName.setVisibility(View.VISIBLE);

            spCategory.setVisibility(View.GONE);
            stvCategory.setVisibility(View.VISIBLE);

            edSize.setVisibility(View.GONE);
            edMaxSize.setVisibility(View.GONE);
            stvSize.setVisibility(View.VISIBLE);
            stvMaxSize.setVisibility(View.VISIBLE);

            spStartDate.setVisibility(View.GONE);
            spEndDate.setVisibility(View.GONE);
            stvPeriod.setVisibility(View.VISIBLE);

            edFullDescription.setVisibility(View.GONE);
            stvFullDescription.setVisibility(View.VISIBLE);

            edFormula.setVisibility(View.GONE);
            stvFormula.setVisibility(View.VISIBLE);


        }


        Button btFragmentOk = v.findViewById(R.id.fbtFragmentOk);
        if (!MainList.isPortraitOrientation(getContext()) && flag.equals("show"))
            btFragmentOk.setVisibility(View.GONE);

        //нажимаем на кнопку ОК
        btFragmentOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String photoPath = "";

                for (String ph : currentPhotos
                ) {
                    photoPath = photoPath + ph + ",";
                }

                if (flag.equals("add")) {
                    ItemClass item = new ItemClass(photoPath,
                            edName.getText().toString(),
                            spCategory.getSelectedItem().toString(),
                            Integer.parseInt(edMaxSize.getText().toString()),
                            Integer.parseInt(edSize.getText().toString()),
                            spStartDate.getSelectedItem().toString(),
                            spEndDate.getSelectedItem().toString(),
                            edFullDescription.getText().toString(),
                            edFormula.getText().toString()
                    );
                    itemClassViewModal.add(item);
                    Log.d("tag", " ONCLICK OK FLAG: " + flag + " NAME " + item.getName().toString() + " id " + id);
                }
                if (flag.equals("edit")) {
                    ItemClass item = new ItemClass(id, photoPath,
                            edName.getText().toString(),
                            spCategory.getSelectedItem().toString(),
                            Integer.parseInt(edMaxSize.getText().toString()),
                            Integer.parseInt(edSize.getText().toString()),
                            spStartDate.getSelectedItem().toString(),
                            spEndDate.getSelectedItem().toString(),
                            edFullDescription.getText().toString(),
                            edFormula.getText().toString()
                    );
                    itemClassViewModal.edit(item);
                    Log.d("tag", " ONCLICK OK FLAG: " + flag + " NAME " + item.getName().toString() + " id " + id);
                }


                getFragmentManager().

                        popBackStack();


            }
        });


        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1111:
                    uri = data.getData();
                    currentPhotos.add(uri.toString());
                    live_photos.setValue(currentPhotos);
                    break;

                case 3333:
                    int pozition = data.getIntExtra("pozition", -1);
                    currentPhotos.remove(pozition);
                    ((LinearLayout) v.findViewById(R.id.llPhotos)).removeAllViewsInLayout();
                    ((LinearLayout) v.findViewById(R.id.llPhotos)).removeAllViewsInLayout();
                    live_photos.setValue(currentPhotos);
                    Log.d("tag", "OK delete " + valueOf(pozition));
                    break;
            }
        }
    }

    //нужен для присоединения к контексту приложения и доступ к ресурсам.
    @Override
    public void onAttach(Context context) {

        super.onAttach(context);


    }


    @Override
    public void onOkClicked(int pozition) {


    }
}
